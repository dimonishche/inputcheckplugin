﻿var patterns = {
	mail: '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$',
	phone: '^\+?[\s\-\(]*?\d*[\s\-\(\)]*?\d*[\s\-\(\)]*?\d*[\s\-\(\)]*?\d*[\s\-\(\)]*?\d*$' ,
	rusWords: '^[А-Яа-яЁё\s]+$',
	engWords: '^[a-zA-Z\s]+$',
	intNumber: '^[0-9]+$',
	doubleNumber: '^[0-9]+\,[0-9]+$',
	colorHex: '^#+([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$',
	url: 'https?://.+'
};

(function( $ ){

	var defaults = {
		input: 0,  //текущий input
		defaultInputStyle : 0, //стиль инпута по умолчанию
		errorInputStyle : {"border-color" : "red"}, //стиль инпута при ошибке css или string
		successInputStyle : 0, //стиль инпута при успешной проверке css или string
		patternError : "Неверные данные", //ошибка при не соответстви паттерну
		checkUnicity : 0,   //массив объектов для проверки уникальности [{name : input[name], message : "", action : "", page : ""}
        inputsPathLevel : "multiLevel", //simple, multiLevel
        inputTypeNotCheck : ["checkbox", "button", "file", "hidden", "image", "radio", "reset", "submit"] //типы инпутов, которые не буду проверятся при вызове checkAll
	};

	var options;

	var methods = {
        //проверка одного инпута, возвращает true или false
		simpleCheck : function(params) { 
				options = $.extend({}, defaults, options, params);
				return checkOneInput(this, options);
			},
        //проверка колекции инпутов возвращает object.result (true or false), если true, то возвращает object.data
        checkAll: function(params) {
            options = $.extend({}, defaults, options, params);
            return checkEveryInput(this, options); 
        }		
	};

	$.fn.checkInput = function(method) {

		defaults.input = this;

		if ( methods[method] ) {
	      	return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
	    } else if ( typeof method === 'object' || ! method ) {
	      	return methods.simpleCheck.apply( this, arguments );
	    } else {
	      	$.error( 'Метод ' +  method + ' в jQuery.plugin не существует' );
	    }
	};
})( jQuery );

//сбрасывание ошибок
function resetError(object, options) {

    if (typeof options.errorInputStyle === 'string' && options.defaultInputStyle == 0)
    	object.removeClass(options.errorInputStyle);

   	else if (typeof options.defaultInputStyle === "string")
    	object.addClass(options.defaultInputStyle);

    else if (typeof options.defaultInputStyle === "object")
    	object.css(options.defaultInputStyle);
}

//создание новой ошибки
function newError(object, options, message) {

    var id = object.attr('id');
    $('p#'+id+'.errorMessage').html(message);

    if (typeof options.errorInputStyle === 'string' && typeof options.successInputStyle === 'string')
    	object.removeClass(options.successInputStyle).addClass(options.errorInputStyle);

    else if (typeof options.errorInputStyle === 'string' && typeof options.successInputStyle === 'object')
    	object.css(options.successInputStyle).addClass(options.errorInputStyle);

    else if (typeof options.errorInputStyle === 'object' && typeof options.successInputStyle === 'object')
    	object.css(options.successInputStyle).css(options.errorInputStyle);

    else if (typeof options.errorInputStyle === 'object' && typeof options.successInputStyle === 'string')
    	object.removeClass(options.successInputStyle).css(options.errorInputStyle);

    else if (typeof options.errorInputStyle === 'string' && options.successInputStyle == 0)
    	object.addClass(options.errorInputStyle);

    else if (typeof options.errorInputStyle === 'object' && options.successInputStyle == 0)
    	object.css(options.errorInputStyle);

}

//отмечаем инпут, или сбарасываем стиль обишки, если прошел проверку
function newSuccess(object, options) {

	var id = object.attr('id');
    $('p#'+id+'').html('');

	if (typeof options.successInputStyle === "string")
 		object.addClass(options.successInputStyle);

 	else if (typeof options.successInputStyle === "object")
 		object.css(options.successInputStyle);

 	else if (options.successInputStyle == 0)
 		resetError(object, options);
}

//проверка длины,если она указана
function checkLength(object, options) {

    var result = true;
    var id = object.attr('id');
    var minLength = object.attr('data-min-length');
    var maxLength = object.attr('data-max-length');
    options.maxLength = maxLength;

    if (object.val().length == 0) {

        newError(object, options, "Введите даные");
        return false;
    }

    else if (minLength != undefined || minLength != false) {

        var length = parseInt(minLength);

        if (object.val().length < length) {
            newError(object, options, "Минимальное количество символов: " + length);
            return  false;
        }
    }

    if (maxLength != undefined || maxLength != false) {

        var length = parseInt(maxLength);

        if (object.val().length > length) {
            newError(object, options, "Максимальное количество символов: " + length);
            return  false;
        }
    }

    return true;
}

//проверка паттернов
function patternCheck(object, options) {

    var pattern = object.attr('data-pattern');
    var data = object.val();

    if (pattern in patterns) pattern = patterns[pattern];

    if (pattern != undefined || pattern != false) {

        pattern = new RegExp(pattern);

        if (!pattern.test(data)) {

        newError(object, options, options.patternError)
        return false;
        }

        else return true;
    }
    return true;
}

//проверка инпутов на равность
function equalCheck(object, options) {

    var equal_to_name = object.attr('data-equal');
    var equal_error_message = object.attr('data-equal-error-message');

    if (equal_to_name != undefined) {

        var data_from = object.val();
        var data_to = $('input[name="' + equal_to_name + '"]').val();
        if (data_from != data_to)
        {
            newError(object, options, equal_error_message)
            return false;
        }
    }

    return true;
}

//проверка инпутов на наличие ошибок
function checkOneInput(object, options) {

    var result = true;
    newSuccess(object, options);

    //проверка длины
    result = checkLength(object, options);
    if (result == false)
        return false;

    //проверка паттернов
    result = patternCheck(object, options);
    if (result == false)
        return false;

    //проверка на равность
    result = equalCheck(object, options);
    if (result == false)
        return false;

    //проверка на уникальность
    if (options.checkUnicity != 0 && $.isArray(options.checkUnicity))
    {
    	options.checkUnicity.forEach(function(item) {

    		if (object.attr('id') == item.name) {

    			result = checkUnicity(object, item.message, item.action, item.page);

    			if (result == false)
        			return false;
    		}
    	});		
    }
    return true;
}

//проверка на уникальность через Ajax
function checkUnicity(object, message, action, page)
{
    var result = 0;
    var data = {};

    data[object.attr('name')] = object.val();

    //вызов обработчика проверки на уникальность
    result = ajax_query({handler : action, data : JSON.stringify(data)}, page);
    result = $.parseJSON(result);

    if (result.result == true)
    {
        newError(object, message);
        return false;
    }
}


//проверка каждого инпута в родителях object
function checkEveryInput(object, options) {
    check = true;
    var arrayData = {};

    //проверка если простой путь 
    if (options.inputsPathLevel == "simple") {

        $(object).find("input").each(function() {

            var input = $(this);

            //проверка, если тип не находится в исключении
            if ($.inArray(input.attr["type"], options.inputTypeNotCheck) == -1) {

                if (checkOneInput(input, options) == false) check = false;

                //запись данных в массив ,если инпут прошел проверку
                if (check != false)
                    arrayData[input.attr('name')] = input.val();
            }
        });
    } 
    //проверка, если многоуровневый путь
    else if (options.inputsPathLevel == "multiLevel") {
        $(object).each(function() {

            var input = $(this).find('input');

            //проверка, если тип не находится в исключении
            if ($.inArray(input.attr["type"], options.inputTypeNotCheck) == -1) {
                    
                    if (checkOneInput(input, options) == false) check = false;

                    //запись данных в массив ,если инпут прошел проверку
                    if (check != false)
                        arrayData[input.attr('name')] = input.val();
            }
        });
    }


    if (check == false)
            return {result : check};
    /*если все инпуты прошли проверку, то возвражаеться объект типа
        {
            result : true,
            data: {
                name : value,
                name : value
                ...
            },
        }
    */    
    else return {result : check, data : arrayData};
}